package com.pierrevilly.sandboxapplication

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SandboxApp: Application()