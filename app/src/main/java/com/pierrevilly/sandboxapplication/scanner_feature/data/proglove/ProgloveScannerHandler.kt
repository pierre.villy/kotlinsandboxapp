package com.pierrevilly.sandboxapplication.scanner_feature.data.proglove

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.pierrevilly.sandboxapplication.scanner_feature.data.Proglove
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerConnection
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerDataMatrix
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.domain.BarcodeType
import com.pierrevilly.sandboxapplication.scanner_feature.domain.ScannerRepository
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.ProgloveScannerStatusEnum
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.TriggerFeedbackEnum
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.BarcodeScanState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class ProgloveScannerHandler @Inject constructor(
    private val scannerRepository: ScannerRepository
): ScannerHandler, ScannerConnection, ScannerDataMatrix, Proglove {

    override var barcodeScanState: BarcodeScanState by mutableStateOf(BarcodeScanState())
        private set

    private val _progloveScannerStatusEnum = MutableStateFlow(ProgloveScannerStatusEnum.DISCONNECTED)
    override val progloveScannerStatusEnum: StateFlow<ProgloveScannerStatusEnum> = _progloveScannerStatusEnum

    @RequiresApi(Build.VERSION_CODES.O)
    override fun handleScanResult(barcodeScanResult: BarcodeScanResult): BarcodeScanState {
        val dateTime: String = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"))

        return try {
            if(barcodeScanResult.symbology.isNullOrEmpty()){
                throw Exception("Type de codebarre nul ou introuvable")
            }
            BarcodeScanState(
                barcodeContent = (if(barcodeScanResult.symbology == "LABEL-TYPE-DATAMATRIX" || barcodeScanResult.symbology == "DATA MATRIX"){
                    decodeDatamatrix(barcodeScanResult.barcodeContent)
                } else {
                    if(barcodeScanResult.barcodeContent.substring(0,1) == "U"){
                        BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent.drop(1))
                    }
                    else if(barcodeScanResult.barcodeContent.substring(0,1) == "C"){
                        BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent.drop(1))
                    } else {
                        BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent)
                    }
                }),
                symbology = barcodeScanResult.symbology,
                isLoading = false,
                dateTime = dateTime,
                errorMessage = null
            )
        } catch (e: Exception){
            BarcodeScanState(
                barcodeContent = if(barcodeScanResult.symbology == "LABEL-TYPE-DATAMATRIX"){
                    decodeDatamatrix(barcodeScanResult.barcodeContent)
                } else {
                    BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent)
                },
                symbology = barcodeScanResult.symbology,
                isLoading = false,
                dateTime = dateTime,
                errorMessage = null
            )
        }
    }

    override fun triggerFeedback(triggerFeedbackEnum: TriggerFeedbackEnum) {
        when(triggerFeedbackEnum){
            TriggerFeedbackEnum.ERROR -> scannerRepository.playError()
            TriggerFeedbackEnum.SUCCESS -> scannerRepository.playSuccess()
            else -> {}
        }
    }

    override fun toggleConnection(context: Context, checked: Boolean) {
        if(checked) connect(context) else disconnect(context)
    }

    override fun decodeDatamatrix(barcodeContent: String): BarcodeType.Datamatrix =
        barcodeContent.split("\u001e", "\u001d").fold(BarcodeType.Datamatrix()){
                decoded, token ->
            when {
                token.startsWith("A") -> decoded.copy(firstData = token.drop(1))
                token.startsWith("B") -> decoded.copy(secondData = token.drop(1))
                token.startsWith("C") -> decoded.copy(thirdData = token.drop(1))
                token.startsWith("D") -> decoded.copy(fourthData = token.drop(1))
                token.startsWith("E") -> decoded.copy(fifthData = token.drop(1))
                else -> decoded
            }
        }

    private fun connect(context: Context) {
        val intent = Intent()
        val component = ComponentName(
            "de.proglove.connect",
            "de.proglove.coreui.activities.PairingActivity"
        )
        intent.component = component
        context.startActivity(intent)
    }

    private fun disconnect(context: Context) {
        val intent = Intent()
        intent.action = "com.proglove.api.DISCONNECT"
        context.sendBroadcast(intent)
    }

    override fun onProgloveScannerStatusChange(progloveScannerStatusEnum: ProgloveScannerStatusEnum) {
        _progloveScannerStatusEnum.update { progloveScannerStatusEnum }
        if(progloveScannerStatusEnum == ProgloveScannerStatusEnum.CONNECTED){
            scannerRepository.onProgloveConnectionChange(true)
        } else {
            scannerRepository.onProgloveConnectionChange(false)
        }
    }
}