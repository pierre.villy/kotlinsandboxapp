package com.pierrevilly.sandboxapplication.scanner_feature.data.zebra

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerDataMatrix
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.domain.BarcodeType
import com.pierrevilly.sandboxapplication.scanner_feature.domain.ScannerRepository
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.TriggerFeedbackEnum
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.BarcodeScanState
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class Zebra2DScannerHandler @Inject constructor(
    private val scannerRepository: ScannerRepository,
): ScannerHandler, ScannerDataMatrix {

    override var barcodeScanState: BarcodeScanState by mutableStateOf(BarcodeScanState())
        private set

    @RequiresApi(Build.VERSION_CODES.O)
    override fun handleScanResult(barcodeScanResult: BarcodeScanResult): BarcodeScanState {
        val dateTime: String = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"))

        return try {
            if(barcodeScanResult.symbology.isNullOrEmpty()){
                throw Exception("Type de codebarre nul ou introuvable")
            }
            BarcodeScanState(
                barcodeContent = (if(barcodeScanResult.symbology == "LABEL-TYPE-DATAMATRIX"){
                    decodeDatamatrix(barcodeScanResult.barcodeContent)
                } else {
                    if(barcodeScanResult.barcodeContent.substring(0,1) == "U"){
                        BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent.drop(1))
                    }
                    else if(barcodeScanResult.barcodeContent.substring(0,1) == "C"){
                        BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent.drop(1))
                    } else {
                        BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent)
                    }
                }),
                symbology = barcodeScanResult.symbology,
                isLoading = false,
                dateTime = dateTime,
                errorMessage = null
            )
        } catch (e: Exception){
            BarcodeScanState(
                barcodeContent = if(barcodeScanResult.symbology == "LABEL-TYPE-DATAMATRIX"){
                    decodeDatamatrix(barcodeScanResult.barcodeContent)
                } else {
                    BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent)
                },
                symbology = barcodeScanResult.symbology,
                isLoading = false,
                dateTime = dateTime,
                errorMessage = null
            )
        }
    }

    override fun triggerFeedback(triggerFeedbackEnum: TriggerFeedbackEnum) {
        TODO("Not yet implemented")
    }

    override fun decodeDatamatrix(barcodeContent: String): BarcodeType.Datamatrix {
        TODO("Not yet implemented")
    }
}