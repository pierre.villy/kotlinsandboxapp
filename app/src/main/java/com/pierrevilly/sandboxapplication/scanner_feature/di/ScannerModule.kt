package com.pierrevilly.sandboxapplication.scanner_feature.di

import android.content.Context
import android.media.MediaPlayer
import com.pierrevilly.sandboxapplication.core.data.preferences.UserPreferencesRepository
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerRepositoryImpl
import com.pierrevilly.sandboxapplication.scanner_feature.data.bluetooth.BluetoothScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.data.proglove.ProgloveScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.data.zebra.Zebra2DScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.domain.ScannerRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ScannerModule {

    @Singleton
    @Provides
    fun provideProgloveScannerHandler(scannerRepository: ScannerRepository): ProgloveScannerHandler {
        return ProgloveScannerHandler(scannerRepository)
    }

    @Singleton
    @Provides
    fun provideBluetoothScannerHandler(scannerRepository: ScannerRepository): BluetoothScannerHandler {
        return BluetoothScannerHandler(scannerRepository)
    }

    @Singleton
    @Provides
    fun provideZebra2DScannerHandler(scannerRepository: ScannerRepository): Zebra2DScannerHandler {
        return Zebra2DScannerHandler(scannerRepository)
    }

    @Provides
    fun provideMediaPlayer(): MediaPlayer {
        return MediaPlayer()
    }

    @Singleton
    @Provides
    fun provideScannerRepository(
        userPreferencesRepository: UserPreferencesRepository,
        errorMediaPlayer: MediaPlayer,
        successMediaPlayer: MediaPlayer,
        @ApplicationContext context: Context
    ): ScannerRepository {
        return ScannerRepositoryImpl(
            userPreferencesRepository,
            errorMediaPlayer,
            successMediaPlayer,
            context
        )
    }
}