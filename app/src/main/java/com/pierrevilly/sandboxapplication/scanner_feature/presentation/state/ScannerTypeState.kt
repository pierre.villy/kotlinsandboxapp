package com.pierrevilly.sandboxapplication.scanner_feature.presentation.state

import com.pierrevilly.sandboxapplication.UserPreferences

class ScannerTypeState (
    val scannerTypeEnum: UserPreferences.ScannerTypeEnum
)