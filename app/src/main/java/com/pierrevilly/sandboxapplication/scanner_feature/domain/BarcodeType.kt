package com.pierrevilly.sandboxapplication.scanner_feature.domain

sealed interface BarcodeType {
    fun getBarcodeValue(): String

    data class Datamatrix(
        val firstData: String = "",
        val secondData: String = "",
        val thirdData: String = "",
        val fourthData: String = "",
        val fifthData: String = "",
    ): BarcodeType{
        override fun getBarcodeValue(): String {
            return "$firstData|$fifthData|$fourthData|$thirdData|$secondData"
        }
    }

    data class DefaultBarcode(
        val data: String
    ): BarcodeType {
        override fun getBarcodeValue(): String {
            return data
        }
    }
}