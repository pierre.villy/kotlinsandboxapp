package com.pierrevilly.sandboxapplication.scanner_feature.domain.model

data class BarcodeScanResult(
    val barcodeContent: String = "",
    val symbology: String?
)
