package com.pierrevilly.sandboxapplication.scanner_feature.presentation.state

import com.pierrevilly.sandboxapplication.scanner_feature.domain.BarcodeType

data class BarcodeScanState(
    val barcodeContent: BarcodeType? = null,
    val symbology: String? = null,
    val dateTime: String = "",
    val errorMessage: String? = null,
    val isLoading: Boolean = false
)
