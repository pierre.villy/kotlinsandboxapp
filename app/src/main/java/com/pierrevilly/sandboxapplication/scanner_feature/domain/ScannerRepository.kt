package com.pierrevilly.sandboxapplication.scanner_feature.domain

import android.content.Context
import androidx.lifecycle.LiveData
import com.pierrevilly.sandboxapplication.UserPreferences.ScannerTypeEnum
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.ProgloveScannerStatusEnum
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.BarcodeScanState
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.ScannerTypeState
import kotlinx.coroutines.flow.StateFlow

interface ScannerRepository {
    val scannerHandler: ScannerHandler?
    val scannerStateFlow: LiveData<ScannerTypeState>
    val barcodeScanState: StateFlow<BarcodeScanState>
    val progloveIsConnected: StateFlow<Boolean>
    val progloveConnectionStatus: StateFlow<ProgloveScannerStatusEnum>

    fun setScannerHandler(scannerTypeEnum: ScannerTypeEnum)
    fun updateBarcodeScanState(barcodeScanResult: BarcodeScanResult)
    fun playError()
    fun playSuccess()
    fun enableProglove(context: Context, enable: Boolean)
    fun resetBarcodeScanResult()
    fun onProgloveConnectionChange(enable: Boolean)
}