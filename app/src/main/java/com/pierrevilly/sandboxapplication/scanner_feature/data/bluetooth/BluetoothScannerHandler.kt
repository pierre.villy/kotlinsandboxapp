package com.pierrevilly.sandboxapplication.scanner_feature.data.bluetooth

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerConnection
import com.pierrevilly.sandboxapplication.scanner_feature.data.ScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.domain.BarcodeType
import com.pierrevilly.sandboxapplication.scanner_feature.domain.ScannerRepository
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.TriggerFeedbackEnum
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.BarcodeScanState
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class BluetoothScannerHandler @Inject constructor(
    private val scannerRepository: ScannerRepository
): ScannerHandler, ScannerConnection {

    //TODO : implement bluetooth scanner

    override var barcodeScanState: BarcodeScanState by mutableStateOf(BarcodeScanState())
        private set

    @RequiresApi(Build.VERSION_CODES.O)
    override fun handleScanResult(barcodeScanResult: BarcodeScanResult): BarcodeScanState {
        val dateTime: String = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"))

        return try {
            if(barcodeScanResult.symbology.isNullOrEmpty()){
                throw Exception("Type de codebarre nul ou introuvable")
            }
            BarcodeScanState(
                barcodeContent = if(barcodeScanResult.barcodeContent.substring(0,1) == "U"){
                    BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent.drop(1))
                }
                else if(barcodeScanResult.barcodeContent.substring(0,1) == "C"){
                    BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent.drop(1))
                } else {
                    BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent)
                },
                symbology = barcodeScanResult.symbology,
                isLoading = false,
                dateTime = dateTime,
                errorMessage = null
            )
        } catch (e: Exception){
            BarcodeScanState(
                barcodeContent = BarcodeType.DefaultBarcode(barcodeScanResult.barcodeContent),
                symbology = barcodeScanResult.symbology,
                isLoading = false,
                dateTime = dateTime,
                errorMessage = null
            )
        }
    }

    override fun triggerFeedback(triggerFeedbackEnum: TriggerFeedbackEnum) {
        when(triggerFeedbackEnum){
            TriggerFeedbackEnum.ERROR -> scannerRepository.playError()
            TriggerFeedbackEnum.SUCCESS -> scannerRepository.playSuccess()
            else -> {}
        }
    }

    override fun toggleConnection(context: Context, checked: Boolean) {
        if(checked) connect(context) else disconnect(context)
    }

    private fun connect(context: Context){

    }

    private fun disconnect(context: Context){

    }

}