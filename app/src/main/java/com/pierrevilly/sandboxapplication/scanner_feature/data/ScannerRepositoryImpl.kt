package com.pierrevilly.sandboxapplication.scanner_feature.data

import android.content.Context
import android.media.MediaPlayer
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import com.pierrevilly.sandboxapplication.R
import com.pierrevilly.sandboxapplication.UserPreferences
import com.pierrevilly.sandboxapplication.UserPreferences.ScannerTypeEnum
import com.pierrevilly.sandboxapplication.core.data.preferences.UserPreferencesRepository
import com.pierrevilly.sandboxapplication.scanner_feature.data.proglove.ProgloveScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.data.zebra.Zebra2DScannerHandler
import com.pierrevilly.sandboxapplication.scanner_feature.domain.ScannerRepository
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.ProgloveScannerStatusEnum
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.BarcodeScanState
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.ScannerTypeState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

class ScannerRepositoryImpl @Inject constructor(
    private val userPreferencesRepository: UserPreferencesRepository,
    private var errorPlayer: MediaPlayer,
    private var successPlayer: MediaPlayer,
    private val context: Context
): ScannerRepository {

    private val userPreferencesFlow = userPreferencesRepository.userPreferencesFlow

    private val scannerStateView =
        combine(
            userPreferencesFlow,
        ){ userPreferences ->
            return@combine ScannerTypeState(
                scannerTypeEnum = userPreferences.first().scannerType
            )
        }

    init {
        val initialSetup = liveData {
            emit(userPreferencesRepository.fetchInitialPreferences())
        }
        if(Build.MANUFACTURER == "Zebra Technologies"){
            setScannerHandler(ScannerTypeEnum.ZEBRA)
        }
        errorPlayer = MediaPlayer.create(context, R.raw.error)
        successPlayer = MediaPlayer.create(context, R.raw.success)
    }

    override val scannerStateFlow: LiveData<ScannerTypeState>
        get() = scannerStateView.asLiveData()

    private var _scannerHandler: ScannerHandler? = null
    override val scannerHandler: ScannerHandler?
        get() = _scannerHandler


    private val _barcodeScanState = MutableStateFlow(BarcodeScanState())
    override val barcodeScanState: StateFlow<BarcodeScanState> = _barcodeScanState

    private val _progloveIsConnected = MutableStateFlow(false)
    override val progloveIsConnected: StateFlow<Boolean> = _progloveIsConnected

    private val _progloveConnectionStatus = MutableStateFlow(ProgloveScannerStatusEnum.DISCONNECTED)
    override val progloveConnectionStatus: StateFlow<ProgloveScannerStatusEnum> = _progloveConnectionStatus

    override fun updateBarcodeScanState(barcodeScanResult: BarcodeScanResult) {
        val barcodeScanState = scannerHandler!!.handleScanResult(barcodeScanResult)
        _barcodeScanState.update { barcodeScanState }
    }

    override fun resetBarcodeScanResult() {
        _barcodeScanState.update { BarcodeScanState() }
    }

    override fun setScannerHandler(scannerTypeEnum: UserPreferences.ScannerTypeEnum) {
        _scannerHandler = when(scannerTypeEnum){
            ScannerTypeEnum.ZEBRA -> Zebra2DScannerHandler(this)
            ScannerTypeEnum.PROGLOVE -> ProgloveScannerHandler(this)
            else -> Zebra2DScannerHandler(this)
        }
        CoroutineScope(Dispatchers.IO).launch {
            userPreferencesRepository.updateScannerType(scannerTypeEnum)
        }
    }
    override fun playError(){
        errorPlayer.start()
    }
    override fun playSuccess(){
        successPlayer.start()
    }

    override fun enableProglove(context: Context, enable: Boolean) {
        if(enable){
            setScannerHandler(ScannerTypeEnum.PROGLOVE)
            (scannerHandler as ProgloveScannerHandler).toggleConnection(context, true)
        } else {
            (scannerHandler as ProgloveScannerHandler).toggleConnection(context, false)
        }
    }

    override fun onProgloveConnectionChange(enable: Boolean) {
        _progloveIsConnected.update { enable }
    }

}