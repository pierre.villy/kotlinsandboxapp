package com.pierrevilly.sandboxapplication.scanner_feature.domain.model

enum class ProgloveScannerStatusEnum {
    CONNECTED,
    DISCONNECTED,
    CONNECTING,
    ERROR,
    RECONNECTING,
    SEARCHING
}