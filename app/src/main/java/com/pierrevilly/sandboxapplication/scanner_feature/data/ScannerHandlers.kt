package com.pierrevilly.sandboxapplication.scanner_feature.data

import android.content.Context
import com.pierrevilly.sandboxapplication.scanner_feature.domain.BarcodeType
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.ProgloveScannerStatusEnum
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.TriggerFeedbackEnum
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.BarcodeScanState
import kotlinx.coroutines.flow.StateFlow

interface ScannerHandler {
    val barcodeScanState: BarcodeScanState
    fun handleScanResult(barcodeScanResult: BarcodeScanResult): BarcodeScanState
    fun triggerFeedback(triggerFeedbackEnum: TriggerFeedbackEnum)
}

interface ScannerConnection {
    fun toggleConnection(context: Context, checked: Boolean)
}

interface ScannerDataMatrix {
    fun decodeDatamatrix(barcodeContent: String) : BarcodeType.Datamatrix
}

interface Proglove {
    val progloveScannerStatusEnum: StateFlow<ProgloveScannerStatusEnum>
    fun onProgloveScannerStatusChange(progloveScannerStatusEnum: ProgloveScannerStatusEnum)
}