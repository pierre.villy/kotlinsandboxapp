package com.pierrevilly.sandboxapplication.scanner_feature.domain.model

enum class TriggerFeedbackEnum {
    SUCCESS,
    ERROR,
    WARNING,
    NOTIFICATION
}