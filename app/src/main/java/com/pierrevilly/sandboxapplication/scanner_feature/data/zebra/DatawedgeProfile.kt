package com.pierrevilly.sandboxapplication.scanner_feature.data.zebra

import android.content.Context
import android.os.Bundle

object DatawedgeProfile {
    private const val PROFILE_NAME = "SandboxApplication"
    private const val PROFILE_INTENT_ACTION = "com.pierrevilly.sandboxapplication.SCAN"
    private const val PROFILE_INTENT_START_ACTIVITY = "2"

    fun createDataWedgeProfile(context: Context) {
        //  Create and configure the DataWedge profile associated with this application
        //  For readability's sake, I have not defined each of the keys in the DWInterface file
        DatawedgeIntentActions().sendCommandString(context, DatawedgeIntentActions.DATAWEDGE_SEND_CREATE_PROFILE,
            PROFILE_NAME
        )
        val profileConfig = Bundle()
        profileConfig.putString("PROFILE_NAME", PROFILE_NAME)
        profileConfig.putString("PROFILE_ENABLED", "true") //  These are all strings
        profileConfig.putString("CONFIG_MODE", "UPDATE")
        val barcodeConfig = Bundle()
        barcodeConfig.putString("PLUGIN_NAME", "BARCODE")
        barcodeConfig.putString("RESET_CONFIG", "true") //  This is the default but never hurts to specify
        val barcodeProps = Bundle()
        barcodeConfig.putBundle("PARAM_LIST", barcodeProps)
        profileConfig.putBundle("PLUGIN_CONFIG", barcodeConfig)
        val appConfig = Bundle()
        appConfig.putString("PACKAGE_NAME", context.packageName)      //  Associate the profile with this app
        appConfig.putStringArray("ACTIVITY_LIST", arrayOf("*"))
        profileConfig.putParcelableArray("APP_LIST", arrayOf(appConfig))
        DatawedgeIntentActions().sendCommandBundle(context, DatawedgeIntentActions.DATAWEDGE_SEND_SET_CONFIG, profileConfig)
        //  You can only configure one plugin at a time in some versions of DW, now do the intent output
        profileConfig.remove("PLUGIN_CONFIG")
        val intentConfig = Bundle()
        intentConfig.putString("PLUGIN_NAME", "INTENT")
        intentConfig.putString("RESET_CONFIG", "true")
        val intentProps = Bundle()
        intentProps.putString("intent_output_enabled", "true")
        intentProps.putString("intent_action", PROFILE_INTENT_ACTION)
        intentProps.putString("intent_delivery",
            PROFILE_INTENT_START_ACTIVITY
        )  //  "0"
        intentConfig.putBundle("PARAM_LIST", intentProps)
        profileConfig.putBundle("PLUGIN_CONFIG", intentConfig)
        DatawedgeIntentActions().sendCommandBundle(context, DatawedgeIntentActions.DATAWEDGE_SEND_SET_CONFIG, profileConfig)
    }
}