package com.pierrevilly.sandboxapplication.core.presentation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun MainScreen(
) {
    val navController: NavHostController = rememberNavController()
    Surface(modifier = Modifier.fillMaxSize()) {
        Navigation(navController)
    }
}
