package com.pierrevilly.sandboxapplication.core.presentation

import android.os.Build
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIos
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Login
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveableStateHolder
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.FloatingWindow
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.compose.LocalOwnersProvider
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.pierrevilly.sandboxapplication.core.presentation.components.ProvideAppBarAction
import com.pierrevilly.sandboxapplication.core.presentation.components.ProvideAppBarTitle
import com.pierrevilly.sandboxapplication.core.presentation.components.TopAppBarCustom
import com.pierrevilly.sandboxapplication.login_feature.presentation.LoginScreen
import kotlinx.coroutines.flow.filterNot

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun Navigation(
    navController: NavHostController,
){
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        topBar = {
            TopAppBarCustom(
                navController = navController,
                scaffoldState = { scaffoldState }
            )
        }
    ) { innerPadding ->
        NavHost(
            navController = navController,
            startDestination = ScreenRoutes.LoginScreen.route,
            modifier = Modifier.padding(innerPadding)
        ){

            /**
             * Home Screen
             */
            composable(ScreenRoutes.HomeScreen.route){
                ProvideAppBarTitle {
                    Text(text = "Home")
                }

                ProvideAppBarAction {
                    IconButton(onClick = { navController.navigate(ScreenRoutes.SettingsScreen.route) }) {
                        Icon(imageVector = Icons.Filled.Settings, contentDescription = "Settings")
                    }
                }
                HomeScreen()
            }

            /**
             * Login Screen
             */
            composable(ScreenRoutes.LoginScreen.route){
                ProvideAppBarTitle {
                    Text(text = "Login")
                }
                LoginScreen(
                    onLoginSuccessNavigation = {
                        navController.navigate(ScreenRoutes.HomeScreen.route) {
                            popUpTo(0)
                        }
                    }
                )
            }

            /**
             * Settings Screen
             */
            composable(ScreenRoutes.SettingsScreen.route){
                ProvideAppBarTitle {
                    Text(text = "Settings")
                }
                SettingsScreen()
            }
        }
    }

}

sealed class ScreenRoutes(val route: String, val title: String, val imageVector: ImageVector? = null ){
    object HomeScreen: ScreenRoutes("home", "Home", Icons.Filled.Home)
    object SettingsScreen: ScreenRoutes("settings", "Settings", Icons.Filled.Settings)
    object LoginScreen: ScreenRoutes("login", "Login", Icons.Filled.Login)
}

@Composable
fun HomeScreen(){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Text(text = "Home", style = MaterialTheme.typography.h4, fontWeight = FontWeight.Bold)
    }

}

@Composable
fun SettingsScreen(){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Text(text = "Settings", style = MaterialTheme.typography.h4, fontWeight = FontWeight.Bold)
    }
}