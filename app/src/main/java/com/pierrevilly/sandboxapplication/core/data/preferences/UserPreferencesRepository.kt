package com.pierrevilly.sandboxapplication.core.data.preferences

import android.util.Log
import androidx.datastore.core.DataStore
import com.pierrevilly.sandboxapplication.UserPreferences
import com.pierrevilly.sandboxapplication.UserPreferences.ScannerTypeEnum
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import java.io.IOException
import javax.inject.Inject

/**
 * Class that handles saving and retrieving user preferences
 */
class UserPreferencesRepository @Inject constructor(
    private val userPreferencesStore: DataStore<UserPreferences>
) {
    private val TAG: String = "UserPreferencesRepo"

    /**
     * Get the user preferences flow.
     */
    val userPreferencesFlow: Flow<UserPreferences> = userPreferencesStore.data
        .catch { exception ->
            if(exception is IOException){
                Log.e(TAG, "Error reading user preferences", exception)
                emit(UserPreferences.getDefaultInstance())
            } else {
                throw exception
            }
        }

    suspend fun updateScannerType(scannerTypeEnum: ScannerTypeEnum){
        userPreferencesStore.updateData { currentPreferences ->
            currentPreferences.toBuilder().setScannerType(scannerTypeEnum).build()
        }
    }

    suspend fun fetchInitialPreferences() = userPreferencesStore.data.first()
}