package com.pierrevilly.sandboxapplication.core.di

import android.os.Build
import androidx.annotation.RequiresApi
import com.pierrevilly.sandboxapplication.core.utils.BasicAuthInterceptor
import com.pierrevilly.sandboxapplication.core.utils.exception.ResultCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

private const val API_BASE_URL = "https://api_url.com/api/"
private const val API_SERVICE_NAME = "Service"

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @RequiresApi(Build.VERSION_CODES.O)
    @Provides
    @Singleton
    fun provideApiRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .client(
                OkHttpClient()
                .newBuilder()
                .addInterceptor(BasicAuthInterceptor())
                .build()
            )
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(ResultCallAdapterFactory())
            .build()
    }
}