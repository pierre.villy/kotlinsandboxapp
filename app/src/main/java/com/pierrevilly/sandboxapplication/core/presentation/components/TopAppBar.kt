package com.pierrevilly.sandboxapplication.core.presentation.components

import android.os.Build
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.ScaffoldState
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.produceState
import androidx.compose.runtime.referentialEqualityPolicy
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveableStateHolder
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.FloatingWindow
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.compose.LocalOwnersProvider
import com.pierrevilly.sandboxapplication.core.presentation.ScreenRoutes
import kotlinx.coroutines.flow.filterNot
import kotlinx.coroutines.launch

@Composable
fun TopAppBarCustom(
    navController: NavHostController,
    scaffoldState: () -> ScaffoldState
){
    val scope = rememberCoroutineScope()
    val currentContentBackStackEntry by produceState(
        initialValue = null as NavBackStackEntry?,
        producer = {
            navController.currentBackStackEntryFlow
                .filterNot { it.destination is FloatingWindow }
                .collect{ value = it }
        }
    )
    TopAppBar(
        navigationIcon = {
            when(currentContentBackStackEntry?.destination?.route){
                ScreenRoutes.LoginScreen.route -> {}
                ScreenRoutes.SettingsScreen.route -> {
                    val backPressDispatcher = LocalOnBackPressedDispatcherOwner.current
                    IconButton(
                        onClick = {
                            backPressDispatcher?.onBackPressedDispatcher?.onBackPressed()
                        },
                        content = {
                            Icon(
                                imageVector = Icons.Default.ArrowBack,
                                contentDescription = "arrow back"
                            )
                        }
                    )
                }
                else -> {
                    IconButton(
                        onClick = {
                            scope.launch{
                                scaffoldState().drawerState.open()
                            }
                        },
                        content = {
                            Icon(
                                imageVector = Icons.Default.Menu,
                                contentDescription = "menu"
                            )
                        }
                    )
                }
            }
        },
        title = {
            AppBarTitle(currentContentBackStackEntry)
        },
        actions = {
//            AppBarAction(currentContentBackStackEntry)
            if(currentContentBackStackEntry?.destination?.route != ScreenRoutes.SettingsScreen.route
                && Build.MANUFACTURER != "Zebra Technologies"){
                IconButton(
                    onClick = {
                        navController.navigate(ScreenRoutes.SettingsScreen.route) {
                            launchSingleTop = true
                        }
                    }
                ) {
                    Icon(imageVector = Icons.Filled.Settings, contentDescription = "Options")
                }
            }
        }
    )
}

@Composable
fun ProvideAppBarAction(actions: @Composable RowScope.() -> Unit){
    if(LocalViewModelStoreOwner.current == null || LocalViewModelStoreOwner.current !is NavBackStackEntry)
        return
    val actionViewModel = viewModel(initializer = {TopAppBarViewModel()})
    SideEffect {
        actionViewModel.actionState = actions
    }
}

@Composable
fun ProvideAppBarTitle(title: @Composable () -> Unit) {
    if (LocalViewModelStoreOwner.current == null || LocalViewModelStoreOwner.current !is NavBackStackEntry)
        return
    val actionViewModel = viewModel(initializer = { TopAppBarViewModel() })
    SideEffect {
        actionViewModel.titleState = title
    }
}

@Composable
fun ProvideFloatingActionButton(floatingActionButton: @Composable () -> Unit){
    if (LocalViewModelStoreOwner.current == null || LocalViewModelStoreOwner.current !is NavBackStackEntry)
        return
    val actionViewModel = viewModel(initializer = { TopAppBarViewModel() })
    SideEffect {
        actionViewModel.floatingActionButtonState = floatingActionButton
    }
}

@Composable
fun ProvideDrawer(drawer: @Composable () -> Unit){
    if (LocalViewModelStoreOwner.current == null || LocalViewModelStoreOwner.current !is NavBackStackEntry)
        return
    val actionViewModel = viewModel(initializer = { TopAppBarViewModel() })
    SideEffect {
        actionViewModel.drawerState = drawer
    }
}

@Composable
fun RowScope.AppBarAction(navBackStackEntry: NavBackStackEntry?) {
    val stateHolder = rememberSaveableStateHolder()
    navBackStackEntry?.LocalOwnersProvider(stateHolder) {
        val actionViewModel = viewModel(initializer = { TopAppBarViewModel() })
        actionViewModel.actionState?.let { it() }
    }
}

@Composable
fun AppBarTitle(navBackStackEntry: NavBackStackEntry?) {
    val stateHolder = rememberSaveableStateHolder()
    navBackStackEntry?.LocalOwnersProvider(stateHolder) {
        val actionViewModel = viewModel(initializer = { TopAppBarViewModel() })
        actionViewModel.titleState?.let { it() }
    }
}

@Composable
fun FloatingActionButton(navBackStackEntry: NavBackStackEntry?){
    val stateHolder = rememberSaveableStateHolder()
    navBackStackEntry?.LocalOwnersProvider(stateHolder){
        val actionViewModel = viewModel(initializer = { TopAppBarViewModel() })
        actionViewModel.floatingActionButtonState?.let { it() }
    }
}

@Composable
fun CustomDrawer(navBackStackEntry: NavBackStackEntry?){
    val stateHolder = rememberSaveableStateHolder()
    navBackStackEntry?.LocalOwnersProvider(stateHolder){
        val actionViewModel = viewModel(initializer = { TopAppBarViewModel() })
        actionViewModel.drawerState?.let { it() }
    }
}

private class TopAppBarViewModel : ViewModel(){
    var titleState by mutableStateOf(null as (@Composable () -> Unit)?, referentialEqualityPolicy())
    var actionState by mutableStateOf(null as (@Composable RowScope.() -> Unit)?, referentialEqualityPolicy())
    var floatingActionButtonState by mutableStateOf(null as (@Composable () -> Unit)?, referentialEqualityPolicy())
    var drawerState by mutableStateOf(null as (@Composable () -> Unit)?, referentialEqualityPolicy())
}