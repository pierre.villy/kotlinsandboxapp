package com.pierrevilly.sandboxapplication.core.presentation.components

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import com.pierrevilly.sandboxapplication.UserPreferences.ScannerTypeEnum
import com.pierrevilly.sandboxapplication.scanner_feature.data.zebra.DatawedgeIntentActions
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.presentation.state.ScannerTypeState

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun SystemBroadcastReceiverHandler(
    state: ScannerTypeState?,
    handleBarcodeScan: (BarcodeScanResult) -> Unit,
){
    when(state?.scannerTypeEnum) {
        ScannerTypeEnum.PROGLOVE -> {
            SystemBroadcastReceiver(systemAction = "com.proglove.api.BARCODE"){
                    receiverState ->
                val action = receiverState?.action ?: return@SystemBroadcastReceiver
                if(action == "com.proglove.api.BARCODE") {
                    handleBarcodeScan(
                        BarcodeScanResult(
                            barcodeContent = receiverState.getStringExtra("com.proglove.api.extra.BARCODE_DATA")!!,
                            symbology = receiverState.getStringExtra("com.proglove.api.extra.BARCODE_SYMBOLOGY")
                        )
                    )
                }
            }
        }
        ScannerTypeEnum.ZEBRA -> {
            SystemBroadcastReceiver(systemAction = "com.pierrevilly.sandboxapplication.SCAN"){
                    receiverState ->
                val action = receiverState?.action ?: return@SystemBroadcastReceiver
                if(action == "com.pierrevilly.sandboxapplication.SCAN") {
                    handleBarcodeScan(
                        BarcodeScanResult(
                            barcodeContent = receiverState.getStringExtra(DatawedgeIntentActions.DATAWEDGE_SCAN_EXTRA_DATA_STRING)!!,
                            symbology = receiverState.getStringExtra(DatawedgeIntentActions.DATAWEDGE_SCAN_EXTRA_LABEL_TYPE)
                        )
                    )
                }
            }
        }
        else -> {}
    }
}