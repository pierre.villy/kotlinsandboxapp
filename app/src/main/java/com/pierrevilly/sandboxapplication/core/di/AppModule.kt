package com.pierrevilly.sandboxapplication.core.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.DataStoreFactory
import androidx.datastore.dataStoreFile
import androidx.datastore.migrations.SharedPreferencesMigration
import androidx.datastore.migrations.SharedPreferencesView
import com.pierrevilly.sandboxapplication.UserPreferences
import com.pierrevilly.sandboxapplication.core.data.preferences.UserPreferencesRepository
import com.pierrevilly.sandboxapplication.core.utils.UserPreferencesSerializer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import javax.inject.Singleton

private const val USER_PREFERENCES_NAME = "user_preferences"
private const val DATA_STORE_FILE_NAME = "user_prefs.pb"
private const val SCANNER_TYPE_KEY = "scanner_type"

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideProtoDataStore(@ApplicationContext appContext: Context): DataStore<UserPreferences> {
        return DataStoreFactory.create(
            serializer = UserPreferencesSerializer,
            produceFile = { appContext.dataStoreFile(DATA_STORE_FILE_NAME) },
            corruptionHandler = null,
            migrations = listOf(
                SharedPreferencesMigration(
                    appContext,
                    USER_PREFERENCES_NAME
                ){ sharedPrefs: SharedPreferencesView, currentData: UserPreferences ->
                    if(currentData.scannerType == UserPreferences.ScannerTypeEnum.UNKNOWN){
                        currentData.toBuilder().setScannerType(
                            UserPreferences.ScannerTypeEnum.valueOf(
                                sharedPrefs.getString(
                                    SCANNER_TYPE_KEY,
                                    UserPreferences.ScannerTypeEnum.NONE.name
                                )!!
                            )
                        ).build()
                    } else {
                        currentData
                    }
                }
            ),
            scope = CoroutineScope(Dispatchers.IO + SupervisorJob())
        )
    }

    @Singleton
    @Provides
    fun provideUserPreferencesRepository(userPreferencesDataStore: DataStore<UserPreferences>): UserPreferencesRepository {
        return UserPreferencesRepository(userPreferencesDataStore)
    }
}