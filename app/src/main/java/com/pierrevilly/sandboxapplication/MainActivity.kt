package com.pierrevilly.sandboxapplication

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.pierrevilly.sandboxapplication.core.presentation.MainScreen
import com.pierrevilly.sandboxapplication.scanner_feature.data.zebra.DatawedgeProfile
import com.pierrevilly.sandboxapplication.ui.theme.SandboxApplicationTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //TODO : move datawedge creation profile
        DatawedgeProfile.createDataWedgeProfile(this);
        setContent {
            SandboxApplicationTheme {
                MainScreen()
            }
        }
    }
}