package com.pierrevilly.sandboxapplication.settings_feature.presentation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.pierrevilly.sandboxapplication.core.presentation.components.SystemBroadcastReceiver
import com.pierrevilly.sandboxapplication.core.presentation.components.SystemBroadcastReceiverHandler
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.ProgloveScannerStatusEnum

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun SettingsScreen(
    settingsViewModel: SettingsViewModel = hiltViewModel()
){
    val state by settingsViewModel.scannerStateFlow.observeAsState()
    val progloveIsConnected by settingsViewModel. progloveIsConnected.collectAsState()
    val progloveStatus by settingsViewModel.progloveStatus.collectAsState()
    val context = LocalContext.current

    SystemBroadcastReceiverHandler(
        state = state,
        handleBarcodeScan = settingsViewModel::handleBarcodeScan,
    )
    SystemBroadcastReceiver(systemAction = "com.proglove.api.SCANNER_STATE"){
            receiverState ->
        val action = receiverState?.action ?: return@SystemBroadcastReceiver
        if(action == "com.proglove.api.SCANNER_STATE") {
            val status: ProgloveScannerStatusEnum = when(receiverState.getStringExtra("com.proglove.api.extra.SCANNER_STATE")!!){
                "DISCONNECTED" -> ProgloveScannerStatusEnum.DISCONNECTED
                "SEARCHING" -> ProgloveScannerStatusEnum.SEARCHING
                "CONNECTING" -> ProgloveScannerStatusEnum.CONNECTING
                "CONNECTED" -> ProgloveScannerStatusEnum.CONNECTED
                "RECONNECTING" -> ProgloveScannerStatusEnum.RECONNECTING
                "ERROR" -> ProgloveScannerStatusEnum.ERROR
                else -> ProgloveScannerStatusEnum.ERROR
            }
            settingsViewModel.onProgloveStatusChange(status)
        }
    }

    // TODO : add bluetooth type
    Column(modifier = Modifier.fillMaxSize()) {
        if(state != null){
            Row(
                modifier = Modifier.padding(10.dp),
                horizontalArrangement = Arrangement.spacedBy(5.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(text = "Proglove")
                Text(text = "Statut : $progloveStatus")
                Switch(
                    checked = progloveIsConnected,
                    onCheckedChange = {
                        settingsViewModel.toggleProglove(it, context)
                    }
                )
            }
        }
    }

}