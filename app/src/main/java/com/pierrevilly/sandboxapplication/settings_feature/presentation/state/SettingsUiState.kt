package com.pierrevilly.sandboxapplication.settings_feature.presentation.state

import com.pierrevilly.sandboxapplication.UserPreferences.ScannerTypeEnum

data class SettingsUiState(
    val scannerTypeEnum: ScannerTypeEnum = ScannerTypeEnum.UNKNOWN,
    val errorMessage: String? = null
)