package com.pierrevilly.sandboxapplication.settings_feature.presentation

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pierrevilly.sandboxapplication.scanner_feature.domain.ScannerRepository
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.BarcodeScanResult
import com.pierrevilly.sandboxapplication.scanner_feature.domain.model.ProgloveScannerStatusEnum
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val scannerRepository: ScannerRepository
): ViewModel() {

    var barcodeScanState = scannerRepository.barcodeScanState
    val scannerStateFlow = scannerRepository.scannerStateFlow
    val progloveIsConnected = scannerRepository.progloveIsConnected
    val progloveStatus = scannerRepository.progloveConnectionStatus

    fun handleBarcodeScan(barcodeScanResult: BarcodeScanResult){
        viewModelScope.launch {
            scannerRepository.updateBarcodeScanState(barcodeScanResult)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun toggleProglove(checked: Boolean, context: Context){
        viewModelScope.launch {
            scannerRepository.enableProglove(context, checked)
        }
    }

    fun onProgloveStatusChange(newState: ProgloveScannerStatusEnum){
        viewModelScope.launch {
            if(newState == ProgloveScannerStatusEnum.CONNECTED){
                scannerRepository.onProgloveConnectionChange(true)
            } else if(newState == ProgloveScannerStatusEnum.DISCONNECTED) {
                scannerRepository.onProgloveConnectionChange(false)
            }
        }
    }
}