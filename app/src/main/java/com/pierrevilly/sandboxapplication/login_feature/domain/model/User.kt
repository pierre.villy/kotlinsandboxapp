package com.pierrevilly.sandboxapplication.login_feature.domain.model

data class User(
    val username: String,
    val password: String
)