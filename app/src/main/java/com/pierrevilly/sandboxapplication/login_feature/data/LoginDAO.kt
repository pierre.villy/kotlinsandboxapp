package com.pierrevilly.sandboxapplication.login_feature.data

import com.pierrevilly.sandboxapplication.login_feature.domain.model.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginDAO {

    @POST("auth/login")
    suspend fun login(@Body credentials: User): Response<Void>
}