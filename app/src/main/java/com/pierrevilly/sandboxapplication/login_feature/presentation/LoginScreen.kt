package com.pierrevilly.sandboxapplication.login_feature.presentation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.RemoveRedEye
import androidx.compose.material.icons.filled.VerifiedUser
import androidx.compose.material.icons.filled.VpnKey
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.pierrevilly.sandboxapplication.core.presentation.components.NavDestinationHelper
import com.pierrevilly.sandboxapplication.login_feature.presentation.component.AuthButton
import com.pierrevilly.sandboxapplication.login_feature.presentation.component.TextEntryModule
import com.pierrevilly.sandboxapplication.ui.theme.white
import com.pierrevilly.sandboxapplication.ui.theme.whiteGray

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun LoginScreen(
    onLoginSuccessNavigation: () -> Unit,
    loginViewModel: LoginViewModel = hiltViewModel()
){
    NavDestinationHelper(
        shouldNavigate = {
            loginViewModel.loginState.isSuccessfullyLoggedIn
        },
        destination = {
            onLoginSuccessNavigation()
        }
    )
    Column(modifier = Modifier.fillMaxSize()) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(white),
        ) {
            LoginContainer(
                usernameValue = {
                    loginViewModel.loginState.usernameInput
                },
                passwordValue = {
                    loginViewModel.loginState.passwordInput
                },
                buttonEnabled = {
                    loginViewModel.loginState.isInputValid
                },
                onUsernameChanged = loginViewModel::onUsernameInputChange,
                onPasswordChanged = loginViewModel::onPasswordInputChange,
                onLoginButtonClick = loginViewModel::onLoginClick,
                isPasswordShown = {
                    loginViewModel.loginState.isPasswordShown
                },
                onTrailingPasswordIconClick = { loginViewModel.onToggleVisualTransformation() },
                errorHint = {
                    loginViewModel.loginState.errorMessageInput
                },
                isLoading = {
                    loginViewModel.loginState.isLoading
                },
                modifier = Modifier
                    .fillMaxWidth(0.9F)
                    .shadow(5.dp, RoundedCornerShape(15.dp))
                    .background(whiteGray, RoundedCornerShape(15.dp))
                    .padding(10.dp, 15.dp, 10.dp, 15.dp)
                    .align(Alignment.Center)
            )
        }
    }
}

@Composable
fun LoginContainer(
    usernameValue: () -> String,
    passwordValue: () -> String,
    buttonEnabled: () -> Boolean,
    onUsernameChanged: (String) -> Unit,
    onPasswordChanged: (String) -> Unit,
    onLoginButtonClick: () -> Unit,
    isPasswordShown: () -> Boolean,
    onTrailingPasswordIconClick: () -> Unit,
    errorHint: () -> String?,
    isLoading: () -> Boolean,
    modifier: Modifier = Modifier
){
    Column(modifier = modifier, verticalArrangement = Arrangement.spacedBy(15.dp)) {
        TextEntryModule(
            modifier = Modifier
                .fillMaxWidth(),
            description = "Username",
            hint = "Write your name",
            textValue = usernameValue(),
            textColor = MaterialTheme.colors.primary,
            cursorColor = MaterialTheme.colors.primary,
            onValueChanged = onUsernameChanged,
            trailingIcon = null,
            onTrailingIconClick = null,
            leadingIcon = Icons.Default.VerifiedUser
        )
        TextEntryModule(
            modifier = Modifier
                .fillMaxWidth(),
            description = "Password",
            hint = "Enter password",
            textValue = passwordValue(),
            textColor = MaterialTheme.colors.primary,
            cursorColor = MaterialTheme.colors.primary,
            onValueChanged = onPasswordChanged,
            trailingIcon = Icons.Default.RemoveRedEye,
            onTrailingIconClick = {
                onTrailingPasswordIconClick()
            },
            leadingIcon = Icons.Default.VpnKey,
            visualTransformation = if(isPasswordShown()){
                VisualTransformation.None
            }else PasswordVisualTransformation(),
            keyboardType = KeyboardType.Password
        )
        Column(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(5.dp)
        ) {
            AuthButton(
                text = "Login",
                backgroundColor = MaterialTheme.colors.secondary,
                contentColor = white,
                enabled = buttonEnabled(),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(45.dp)
                    .shadow(5.dp, RoundedCornerShape(25.dp)),
                onButtonClick = onLoginButtonClick,
                isLoading = isLoading(),
            )
            Text(
                text = errorHint() ?: "",
                style = MaterialTheme.typography.caption
            )
        }
    }
}