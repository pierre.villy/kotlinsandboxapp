package com.pierrevilly.sandboxapplication.login_feature.di

import com.pierrevilly.sandboxapplication.login_feature.data.LoginDAO
import com.pierrevilly.sandboxapplication.login_feature.domain.LoginRepository
import com.pierrevilly.sandboxapplication.login_feature.domain.use_case.ValidateLoginInputUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LoginModule {

    @Provides
    @Singleton
    fun provideValidateLoginInputUseCase(): ValidateLoginInputUseCase{
        return ValidateLoginInputUseCase()
    }

    @Provides
    @Singleton
    fun provideLoginDao(retrofit: Retrofit): LoginDAO {
        return retrofit.create(LoginDAO::class.java)
    }

    @Provides
    @Singleton
    fun provideLoginRepository(dao: LoginDAO): LoginRepository {
        return LoginRepository(dao)
    }
}