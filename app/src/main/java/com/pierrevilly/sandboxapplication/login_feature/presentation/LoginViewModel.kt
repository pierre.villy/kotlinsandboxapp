package com.pierrevilly.sandboxapplication.login_feature.presentation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pierrevilly.sandboxapplication.core.utils.BasicAuthInterceptor
import com.pierrevilly.sandboxapplication.login_feature.domain.LoginRepository
import com.pierrevilly.sandboxapplication.login_feature.domain.model.LoginInputValidationType
import com.pierrevilly.sandboxapplication.login_feature.domain.model.User
import com.pierrevilly.sandboxapplication.login_feature.domain.use_case.ValidateLoginInputUseCase
import com.pierrevilly.sandboxapplication.login_feature.presentation.state.LoginState
import com.pierrevilly.sandboxapplication.scanner_feature.domain.ScannerRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.Base64
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val validateLoginInputUseCase: ValidateLoginInputUseCase,
    private val loginRepository: LoginRepository,
    private val scannerRepository: ScannerRepository
): ViewModel() {

    var loginState by mutableStateOf(LoginState())
        private set

    fun onUsernameInputChange(newValue: String){
        loginState = loginState.copy(usernameInput = newValue)
        checkInputValidation()
    }

    fun onPasswordInputChange(newValue: String){
        loginState = loginState.copy(passwordInput = newValue)
        checkInputValidation()
    }

    fun onToggleVisualTransformation(){
        loginState = loginState.copy(isPasswordShown =  !loginState.isPasswordShown)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun onLoginClick(){
        loginState = loginState.copy(isLoading = true)
        viewModelScope.launch {
            loginState = try {
                val credentials = User(
                    username = loginState.usernameInput,
                    password = Base64.getEncoder().encodeToString(loginState.passwordInput.toByteArray())
                )

                val response = loginRepository.login(credentials = credentials)
                if(!response.isSuccessful){
                    throw Exception(response.errorBody()!!.string())
                }
                BasicAuthInterceptor.setCredentials(credentials.username, loginState.passwordInput)
                loginState.copy(isSuccessfullyLoggedIn = true)
            } catch (e: Exception){
                scannerRepository.playError()
                loginState.copy(errorMessageInput = e.message, isLoading = false)
            }
        }
    }

    private fun checkInputValidation(){
        val validationResult = validateLoginInputUseCase(
            loginState.usernameInput,
            loginState.passwordInput
        )
        processInputValidationType(validationResult)
    }

    private fun processInputValidationType(type: LoginInputValidationType){
        loginState = when(type){
            LoginInputValidationType.EmptyField -> {
                loginState.copy(errorMessageInput = "Champs vides restants", isInputValid = false)
            }
            LoginInputValidationType.Valid -> {
                loginState.copy(errorMessageInput = null, isInputValid = true)
            }
        }
    }
}