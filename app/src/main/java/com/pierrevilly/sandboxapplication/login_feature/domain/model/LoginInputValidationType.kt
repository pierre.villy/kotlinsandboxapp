package com.pierrevilly.sandboxapplication.login_feature.domain.model

enum class LoginInputValidationType {
    EmptyField,
    Valid
}