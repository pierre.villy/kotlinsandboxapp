package com.pierrevilly.sandboxapplication.login_feature.domain

import com.pierrevilly.sandboxapplication.login_feature.data.LoginDAO
import com.pierrevilly.sandboxapplication.login_feature.domain.model.User
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import javax.inject.Inject

@Module
@InstallIn(ActivityRetainedComponent::class)
class LoginRepository @Inject constructor(
    private val loginDAO: LoginDAO
) {
    suspend fun login(credentials: User) = loginDAO.login(credentials)
}