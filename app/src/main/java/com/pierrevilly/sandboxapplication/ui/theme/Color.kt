package com.pierrevilly.sandboxapplication.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val orange = Color(0xFFdb660d)
val white = Color(0xFFF3F3F3)
val whiteGray = Color(0xFFd1cdcb)