Cette application me sert de bac à sable sur l'environnement Android avec Kotlin.

## Fonctionnalités : 
- Préférences utilisateurs : choix entre différents types de scanners de code-barres : Zebra 2D, Zebra 1D (bluetooth) et Proglove
- Connexion
- Gestion des résultats de scan par Intent (Zebra 2D, Proglove) ou Bluetooth (Zebra 1D)
- Intéropérabilité : les trois types de scanners peuvent être utilsés de façon transparente grâce à une couche d'abstraction

## Technologies : 
- Kotlin
- Jetpack Compose
- Dagger
- Hilt
- Retrofit
- Protobuf & DataStore
